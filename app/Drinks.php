<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drinks extends Model
{
    protected $fillable = ['title', 'description', 'coffee_amount'];

}
