<?php

namespace App\Http\Controllers;

use App\Drinks;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    // Get Full List of available Drinks from DB
    public function getAll()
    {
        $drinks = Drinks::all();
        return $drinks->toJson();
    }


    // Actual Check Logic
    public function submitCheck(Request $request)
    {

        $validatedData = $request->validate([
            'drink' => 'required|numeric',
            'amount' => 'required|integer|max:10|min:0',
        ]);


        $maxSaveAmount = 500;
        $consumedAmount = ($request->drink*$request->amount);

        if($consumedAmount <= $maxSaveAmount){

            $message = " You are within safe caffeine limits! ";
            $totalAllowed = floor($maxSaveAmount/$request->drink)-$request->amount;

            if($totalAllowed >= 1)
            {
                $message .= " You can have " . $totalAllowed . " more! ";
            }else{
                $message .= "You cannot have any more of your favorite drinks :(";
            }

        }else{
            $message = "Not Safe Limit Reached!";
        }

        return $message;
    }
}
