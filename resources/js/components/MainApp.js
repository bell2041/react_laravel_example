import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import axios from "axios";

class MainApp extends Component {
    constructor (props) {
        super(props)
        this.state = {
            listDrinks: [],
            drink: '',
            amount:'',
            responseText:'',
            errors: []
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleCheck = this.handleCheck.bind(this);

        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleChange(event) {

        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

    }

    handleCheck(event){
        event.preventDefault()
        const data = {
            drink: this.state.drink,
            amount: this.state.amount
        }

        axios.post('/api/submitCheck', data)
            .then(response => {
                this.setState({
                    responseText: response.data,
                    errors: []
                });

            })
            .catch(error => {
                this.setState({
                    errors: error.response.data.errors,
                    responseText: ''
                })
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }
    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }


    componentDidMount () {
        axios.get('/api/getDrinks').then(response => {
            this.setState({
                listDrinks: response.data
            })
        })
    }

    render () {
        const { listDrinks } = this.state
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-8'>
                        <div className='card'>
                            <div className='card-header'>Can I have more coffee?</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCheck}>
                                    <fieldset>
                                        <div className="form-group">
                                            <label className="control-label col-4">Select Your Drink</label>
                                            <div className="col-6">
                                                <select name="drink" value={this.state.drink} onChange={this.handleChange} className={`form-control ${this.hasErrorFor('drink') ? 'is-invalid' : ''}`} >
                                                    <option>--</option>
                                                    {listDrinks.map(drink => (
                                                        <option key={drink.id} value={drink.coffee_amount}>{drink.title} ({drink.coffee_amount}ml)</option>
                                                    ))}
                                                </select>
                                                {this.renderErrorFor('drink')}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="control-label col-lg-8">Enter Consumed Amount </label>
                                            <div className="col-lg-10">
                                                <div className="input-group">
                                                    <input type="text" name="amount" value={this.state.amount} className={`form-control ${this.hasErrorFor('amount') ? 'is-invalid' : ''}`}onChange={this.handleChange}/>
                                                    {this.renderErrorFor('amount')}
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div className="text-center">
                                        <div>{this.state.responseText}</div>
                                        <br />
                                        <button className="btn btn-primary">Check</button>
                                    </div>
                                </form>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(<MainApp />, document.getElementById('app'))
